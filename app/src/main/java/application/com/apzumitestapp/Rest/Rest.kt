package application.com.apzumitestapp.Rest

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Said Murvatov on 11.08.2018.
 */
object Rest {

    private val BITBUCKET_BASE_URL = "https://api.bitbucket.org/2.0/"
    private val GITHUB_BASE_URL = "https://api.github.com/"

    //region Bitbucket Api
    fun getBitbucketApi(): BitbucketApiService {

        val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(
                        RxJava2CallAdapterFactory.create())
                .addConverterFactory(
                        GsonConverterFactory.create())
                .baseUrl(BITBUCKET_BASE_URL)
                .build()

        return retrofit.create(BitbucketApiService::class.java)
    }
    //endregion

    //region Github Api
    fun getGithubApi(): GithubApiService {

        val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(
                        RxJava2CallAdapterFactory.create())
                .addConverterFactory(
                        GsonConverterFactory.create())
                .baseUrl(GITHUB_BASE_URL)
                .build()

        return retrofit.create(GithubApiService::class.java)
    }
    //endregion
}