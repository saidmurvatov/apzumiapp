package application.com.apzumitestapp.Rest

import application.com.apzumitestapp.Models.GithubRepo
import io.reactivex.Observable
import retrofit2.http.GET

/**
 * Created by Said Murvatov on 11.08.2018.
 */
interface GithubApiService {

    @GET("repositories")
    fun getGithubRepo(): Observable<ArrayList<GithubRepo>>

}