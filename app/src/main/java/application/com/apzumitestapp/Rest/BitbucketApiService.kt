package application.com.apzumitestapp.Rest

import application.com.apzumitestapp.Models.BitbucketRepo
import io.reactivex.Observable
import retrofit2.http.GET

/**
 * Created by Said Murvatov on 11.08.2018.
 */
interface BitbucketApiService {
    @GET("repositories?fields=values.name,values.owner,values.description")
    fun getBitbucketRepo(): Observable<BitbucketRepo>

}