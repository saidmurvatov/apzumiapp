package application.com.apzumitestapp.Models

import android.os.Parcel
import android.os.Parcelable
import android.view.View

/**
 * Created by Said Murvatov on 10.08.2018.
 */
data class Repository(var repoName: String,
                      var userName: String,
                      var desciption: String?,
                      var avatarUrl: String,
                      var isGit: Boolean) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readByte() != 0.toByte())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(repoName)
        parcel.writeString(userName)
        parcel.writeString(desciption)
        parcel.writeString(avatarUrl)
        parcel.writeByte(if (isGit) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Repository> {
        override fun createFromParcel(parcel: Parcel): Repository {
            return Repository(parcel)
        }

        override fun newArray(size: Int): Array<Repository?> {
            return arrayOfNulls(size)
        }
    }
}

