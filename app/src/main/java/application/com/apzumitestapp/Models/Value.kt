package application.com.apzumitestapp.Models


data class Value(val owner: RepoOwner, val name: String, val description: String) : Repo {

    override fun parseRepoToRepository(): Repository {
        return Repository(name, owner.username, description, owner.links.avatar.href, false)
    }
}