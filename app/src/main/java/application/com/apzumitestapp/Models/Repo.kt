package application.com.apzumitestapp.Models

/**
 * Created by Said Murvatov on 11.08.2018.
 */
interface Repo {
    fun parseRepoToRepository(): Repository
}