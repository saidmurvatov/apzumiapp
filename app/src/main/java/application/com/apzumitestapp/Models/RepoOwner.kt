package application.com.apzumitestapp.Models


data class RepoOwner(
        val username: String,
        val display_name: String,
        val links: Links
)