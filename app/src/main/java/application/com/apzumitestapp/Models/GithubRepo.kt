package application.com.apzumitestapp.Models


data class GithubRepo(val id: Int, val name: String, val full_name: String, val owner: Owner, val description: String?) : Repo {

    override fun parseRepoToRepository(): Repository {
        return Repository(name, owner.login, description, owner.avatar_url, true)
    }

}