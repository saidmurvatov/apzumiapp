package application.com.apzumitestapp.Models


data class Owner(
        val login: String,
        val avatar_url: String
)