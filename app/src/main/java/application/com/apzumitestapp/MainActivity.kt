package application.com.apzumitestapp

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import application.com.apzumitestapp.Models.Repository
import sigare.com.lazyrecycleradapter.LazyAdapter
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

class MainActivity : BaseActivity() {

    //region Variables
    private val ERROR_RETROFIT = "Error loading the data, please restart the app."

    private val repositories: ArrayList<Repository> = ArrayList()
    private val sortedRepository: ArrayList<Repository> by lazy {
        ArrayList<Repository>(repositories.sortedWith(compareBy(String.CASE_INSENSITIVE_ORDER) { it.repoName }))
    }
    private val lazyAdapter = LazyAdapter(R.layout.lazy_recycler_item_view, this)
    //endregion

    //region onCreate & onResume
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        activity_name.text = resources.getText(R.string.repositories)
        sorting_switch.isEnabled = false

        sorting_switch.setOnCheckedChangeListener { _, _ ->
            if (sorting_switch.isChecked) {
                lazyAdapter.setData(sortedRepository)
            } else {
                lazyAdapter.setData(repositories)
            }
        }

        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL

        recycler_view.layoutManager = linearLayoutManager
        recycler_view.adapter = lazyAdapter
    }


    override fun onResume() {
        super.onResume()
        EventBus.getDefault().register(this)

        makeCall()
    }
    //endregion

    //region Api Call
    private fun makeCall() {
        if (repositories.isEmpty()) {
            getBitRepos(
                    Consumer {
                        it.values.forEach { repositories.add(it.parseRepoToRepository()) }
                        lazyAdapter.setData(repositories)
                        sorting_switch.isEnabled = true
                    },

                    Consumer {
                        Toast.makeText(this, ERROR_RETROFIT, Toast.LENGTH_LONG).show()

                    }
            )

            getGitRepos(
                    Consumer {
                        it.forEach { repositories.add(it.parseRepoToRepository()) }
                        lazyAdapter.setData(repositories)
                        sorting_switch.isEnabled = true
                    },

                    Consumer {
                        Toast.makeText(this, it.localizedMessage, Toast.LENGTH_LONG).show()
                    }
            )
        }
    }
    //endregion

    //region EventBus Handler
    //Event Bus used here. please read https://github.com/greenrobot/EventBus
    @Subscribe
    fun handleSomethingElse(event: String) {
        if (event == NETWORK_CHANGE) {
            makeCall()
            hideNoInternetDialog()
        }
    }
    //endregion

    //region onPause
    override fun onPause() {
        super.onPause()
        EventBus.getDefault().unregister(this)
    }
    //endregion
}
