package sigare.com.lazyrecycleradapter


import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import application.com.apzumitestapp.Models.Repository
import application.com.apzumitestapp.RepositoryDetailsActivity


/**
 * Created by Said Murvatov on 23.03.2018.
 */

class LazyAdapter(private val layout: Int, private val context: Context) : RecyclerView.Adapter<LazyAdapter.ViewHolder>() {
    var model: ArrayList<Repository> = ArrayList()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(model[position])

        holder.itemView.setOnClickListener {
            val intent = Intent(context, RepositoryDetailsActivity::class.java)
            intent.putExtra("data", model[position])
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return model.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = parent.inflate()

        return ViewHolder(view)
    }

    fun setData(data: ArrayList<Repository>) {
        model = data
        notifyDataSetChanged()
    }

    class ViewHolder(private var binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(model: Repository) {
            binding.setVariable(1, model)
        }
    }

    private fun ViewGroup.inflate(): ViewDataBinding {
        val layoutInflater = LayoutInflater.from(context)
        if (itemCount == 0) {
            return DataBindingUtil.inflate(layoutInflater, layout, this, false)
        }
        return DataBindingUtil.inflate(layoutInflater, layout, this, false)
    }
}