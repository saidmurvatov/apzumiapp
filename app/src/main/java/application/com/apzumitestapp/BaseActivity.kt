package application.com.apzumitestapp

import android.app.Dialog
import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import application.com.apzumitestapp.Models.BitbucketRepo
import application.com.apzumitestapp.Models.GithubRepo
import io.reactivex.disposables.Disposable
import application.com.apzumitestapp.Rest.Rest
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import android.view.Window


/**
 * Created by Said Murvatov on 11.08.2018.
 */
open class BaseActivity : AppCompatActivity() {

    //region Variables
    val NETWORK_CHANGE = "android.net.conn.CONNECTIVITY_CHANGE"

    val bitbucketApiService by lazy {
        Rest.getBitbucketApi()
    }
    val githubApiService by lazy {
        Rest.getGithubApi()
    }
    var disposableBitbucketRepo: Disposable? = null
    var disposableGithubRepo: Disposable? = null

    lateinit var dialog: Dialog
    //endregion

    //region onCreate
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.no_internet_layout)
        dialog.setCancelable(false)
    }
    //endregion

    //region Get Data
    fun getBitRepos(response: Consumer<BitbucketRepo>, error: Consumer<Throwable>) {
        if (!isOnline()) {
            showNoInternetDialog()
        }
        disposableBitbucketRepo =
                bitbucketApiService.getBitbucketRepo()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                response,
                                error
                        )

    }

    fun getGitRepos(response: Consumer<ArrayList<GithubRepo>>, error: Consumer<Throwable>) {
        if (!isOnline()) {
            showNoInternetDialog()

        }
        disposableGithubRepo =
                githubApiService.getGithubRepo()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                response,
                                error
                        )

    }
    //endregion

    //region Network State Checking
    fun isOnline(): Boolean {
        val connMgr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }
    //endregion

    //region Show & Hide Dialog
    fun showNoInternetDialog() {
        if (!dialog.isShowing) {
            dialog.show()
        }
    }

    fun hideNoInternetDialog() {
        if (dialog.isShowing) {
            dialog.dismiss()
        }
    }
    //endregion

    //region onPause
    override fun onPause() {
        super.onPause()
        dialog.dismiss()
        disposableGithubRepo?.dispose()
        disposableBitbucketRepo?.dispose()
    }
    //endregion
}