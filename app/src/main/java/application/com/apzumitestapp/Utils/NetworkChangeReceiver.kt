package application.com.apzumitestapp.Utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.util.Log
import org.greenrobot.eventbus.EventBus

/**
 * Created by Said Murvatov on 11.08.2018.
 */
open class NetworkChangeReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {

        if ("android.net.conn.CONNECTIVITY_CHANGE" == intent.action) {
            EventBus.getDefault().post(intent.action)
        }
    }
}