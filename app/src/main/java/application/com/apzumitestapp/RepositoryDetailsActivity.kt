package application.com.apzumitestapp

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import application.com.apzumitestapp.Models.Repository
import kotlinx.android.synthetic.main.custom_toolbar.*

class RepositoryDetailsActivity : AppCompatActivity() {

    //region onCreate
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val dataBinding = DataBindingUtil.setContentView<ViewDataBinding>(this, R.layout.activity_repository_details)

        if (intent != null && intent.extras.get("data") != null) {
            dataBinding.setVariable(1, intent.extras.get("data"))
            activity_name.text = (intent.extras.get("data") as? Repository)?.repoName
            sorting_switch.visibility = View.GONE
        } else {
            finish()
        }


    }
    //endregion
}
