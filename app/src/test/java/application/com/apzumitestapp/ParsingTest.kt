package application.com.apzumitestapp

import org.junit.Test
import application.com.apzumitestapp.Models.*
import org.junit.Assert.*

/**
 * Created by Said Murvatov on 12.08.2018.
 */
class ParsingTest {

    //region Test Data
    private val githubRepos = arrayOf(GithubRepo(1, "Szymon", "Szymon Januszewicz", Owner("szymon", "https://szymon.png"), "this is szymon's app"),
            GithubRepo(1, "Janusz", "Janusz Januszewicz", Owner("janusz", "https://janusz.png"), "this is janusz's app"),
            GithubRepo(1, "Grazyna", "Grazyna Januszewicz", Owner("grazyna", "https://grazyna.png"), "this is grazyna's app"),
            GithubRepo(1, "Brian", "Brian Januszewicz", Owner("brian", "https://brian.png"), "this is brian's app"),
            GithubRepo(1, "Dzesika", "Dzesika Januszewicz", Owner("dzesika", "https://dzesika.png"), "this is dzesika's app"),
            GithubRepo(1, "Sebastian", "Sebastian Januszewicz", Owner("seba", "https://sebastian.png"), "this is seba's app"))
    private val bitbucketRepos = BitbucketRepo(arrayListOf(Value(RepoOwner("szymon", "Szymon Januszewicz", Links(Avatar("https://szymon.png"))), "Szymon", "this is szymon's app"),
            Value(RepoOwner("janusz", "Janusz Januszewicz", Links(Avatar("https://janusz.png"))), "Janusz", "this is janusz's app"),
            Value(RepoOwner("grazyna", "Grazyna Januszewicz", Links(Avatar("https://grazyna.png"))), "Grazyna", "this is grazyna's app"),
            Value(RepoOwner("brian", "Brian Januszewicz", Links(Avatar("https://brian.png"))), "Brian", "this is brian's app"),
            Value(RepoOwner("dzesika", "Dzesika Januszewicz", Links(Avatar("https://dzesika.png"))), "Dzesika", "this is dzesika's app"),
            Value(RepoOwner("seba", "Sebastian Januszewicz", Links(Avatar("https://sebastian.png"))), "Sebastian", "this is seba's app")))

    //endregion

    //region Testitude
    @Test
    fun parsingTest1() {
        for (i in 0 until 6) {
            println(githubRepos[i].parseRepoToRepository().toString())
            println(bitbucketRepos.values[i].parseRepoToRepository().toString())
            val repo = githubRepos[i].parseRepoToRepository()
            repo.isGit = false
            assertEquals(repo, bitbucketRepos.values[i].parseRepoToRepository())
        }
    }
    //endregion
}