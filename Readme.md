# Appzumi Test App

This is the test app provided by Apzumi.

## Getting Started

Please simple checkout the master for the latest code.

### Prerequisites

Android Studio

## Running the tests

There is only one unit test checking the parsing of models and mapping them.


## Authors

* **Said Murvatov**

## Notes

Please reffer to https://medium.com/@saidmurvatov/lazy-recycler-view-adapter-using-databinding-on-android-7dd76475353f

in order to understand the Lazy Adapter used in the app.

The was many possible ways to build this app, many decisions was taken because of simplicity of the app. (not using Dagger) (using EventBus only because it is simple app) (not using the database to save the data for offline usage) and etc.
